import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Hello0000! I am Create React App with Typescript!!/i);
  expect(linkElement).toBeInTheDocument();
});
